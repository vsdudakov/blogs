#-*- encoding: utf-8 -*-
from django.contrib import admin

from main.admin import BaseModelAdmin
from accounting.models import Balance, Transaction


class BalanceAdmin(BaseModelAdmin):
    list_display = (
        'name',
        'get_balance',
    )
    search_fields = ('name',)


class TransactionAdmin(BaseModelAdmin):
    list_display = (
        'creation_date',
        'amount',
        'balance',
        'recipient',
    )
    list_filter = (
        'recipient',
        'balance__name'
    )
    fieldsets = (
        (None, {
            'fields': (
                'amount',
                'balance',
                'recipient',
                'description'
            )
        }),
    )
    search_fields = ('recipient', 'description', 'balance__name')


admin.site.register(Balance, BalanceAdmin)
admin.site.register(Transaction, TransactionAdmin)
