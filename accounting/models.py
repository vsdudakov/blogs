#-*- encoding: utf-8 -*-
from __future__ import unicode_literals
from ckeditor_uploader.fields import RichTextUploadingField

from django.db import models
from django.db.models import Sum
from django.utils.translation import ugettext_lazy as _ul

from main.models import BaseDateModel


class Balance(BaseDateModel):
    name = models.CharField(verbose_name=_ul(u'Название'), max_length=255, unique=True)

    def get_balance(self):
        return Transaction.objects.filter(
            balance=self
        ).aggregate(Sum('amount')).get('amount__sum') or 0

    class Meta:
        ordering = ('-creation_date',)
        verbose_name = _ul(u'Баланс')
        verbose_name_plural = _ul(u'Балансы')

    def __unicode__(self):
        return u'%s' % self.name


class Transaction(BaseDateModel):
    amount = models.DecimalField(verbose_name=_ul(u'Сумма'), max_digits=16, decimal_places=2)
    balance = models.ForeignKey(Balance, verbose_name=_ul(u'Баланс'))

    recipient = models.CharField(verbose_name=_ul(u'Получатель'), max_length=255)
    description = RichTextUploadingField(verbose_name=_ul(u'Описание'), null=True, blank=True)

    class Meta:
        ordering = ('-creation_date',)
        verbose_name = _ul(u'Транзакция')
        verbose_name_plural = _ul(u'Транзакции')

    def __unicode__(self):
        return u'%s: %s' % (self.balance.name, self.amount)