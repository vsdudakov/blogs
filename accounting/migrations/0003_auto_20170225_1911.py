# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-25 19:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0002_auto_20170225_1909'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='amount',
            field=models.DecimalField(decimal_places=2, max_digits=16, verbose_name='\u0421\u0443\u043c\u043c\u0430'),
        ),
    ]
