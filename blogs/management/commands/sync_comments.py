#-*- encoding: utf-8
from django.core.management.base import BaseCommand, CommandError

from blogs.models import Post, Comment


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        for post in Post.objects.all():
            post.num_comments = post.comments().count()
            post.save(update_fields=['num_comments'])

