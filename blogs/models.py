#-*- encoding: utf-8 -*-
from mptt.models import MPTTModel
from mptt.fields import TreeForeignKey
from ckeditor_uploader.fields import RichTextUploadingField

from django.db import models
from django.utils.translation import ugettext_lazy as _ul
from django.utils import timezone

from main.models import BaseDateModel, BaseModel, BaseSeoModel, BasePostModel
from users.models import User


class PostMixin(object):

    def view_action(self, session):
        if self.has_session_key(session, 'post_view', setup=True):
            return
        self.view_count += 1
        self.save(update_fields=['view_count'])

    def like_action(self, session):
        if self.has_session_key(session, 'post_like', setup=True):
            return
        self.rate += 1
        self.save(update_fields=['rate'])

    def unlike_action(self, session):
        if self.has_session_key(session, 'post_like', setup=True):
            return
        self.rate -= 1
        self.save(update_fields=['rate'])

    def post_mini_shorter(self):
        return unicode(self.announcement)[:100] + u"..."

    def is_question(self):
        return self.category.categorytype == Category.CATEGORY_QUESTIONS


class CommentMixin(object):

    def like_action(self, session):
        if self.has_session_key(session, 'comment_like', setup=True):
            return
        self.rate += 1
        self.save(update_fields=['rate'])

    def unlike_action(self, session):
        if self.has_session_key(session, 'comment_like', setup=True):
            return
        self.rate -= 1
        self.save(update_fields=['rate'])


class Tag(BaseDateModel):
    name = models.CharField(verbose_name=_ul(u'Имя'), max_length=255, unique=True)

    class Meta:
        ordering = ('-creation_date',)
        verbose_name = _ul(u'Метка')
        verbose_name_plural = _ul(u'Метки')

    def __unicode__(self):
        return u'%s' % self.name


class Category(BaseSeoModel):
    CATEGORY_NONE = 0
    CATEGORY_QUESTIONS = 1
    CATEGORY_NEWS = 2
    CATEGORY_PRACTICS = 3
    CATEGORY_TYPES = (
        (CATEGORY_NONE, _ul(u'Обычная категория')),
        (CATEGORY_QUESTIONS, _ul(u'Категория вопроса')),
        (CATEGORY_NEWS, _ul(u'Категория новости')),
        (CATEGORY_PRACTICS, _ul(u'Категория практики')),
        )

    name = models.CharField(verbose_name=_ul(u'Имя'), max_length=255, unique=True)
    categorytype = models.PositiveSmallIntegerField(verbose_name=_ul(u'Тип категории'), choices=CATEGORY_TYPES, default=CATEGORY_NONE, db_index=True)
    description = models.TextField(verbose_name=_ul(u'Описание'), null=True, blank=True)
    image_class = models.CharField(verbose_name=_ul(u'Класс картинки'), max_length=255, null=True, blank=True)

    def get_absolute_url(self):
        if self.is_published:
            return u'/category/%s/' % self.slug

    class Meta:
        ordering = ('-creation_date',)
        verbose_name = _ul(u'Категория')
        verbose_name_plural = _ul(u'Категории')

    def __unicode__(self):
        return u'%s' % self.name


class Post(PostMixin, BasePostModel):
    category = models.ForeignKey(Category, verbose_name=_ul(u'Категория'))
    tags = models.ManyToManyField(Tag, verbose_name=_ul(u'Метки'), blank=True)
    author = models.ForeignKey(User, verbose_name=_ul(u'Автор статьи'), related_name='author_posts')
    picture = models.ImageField(verbose_name=_ul(u'Главная картинка'), upload_to=u'uploads/posts/', null=True, blank=True)
    notes = RichTextUploadingField(verbose_name=_ul(u'Notes'), help_text=_ul(u'Notes'), null=True, blank=True)

    def comments(self):
        return Comment.objects.filter(post=self, is_published=True)

    def get_absolute_url(self):
        if self.is_published:
            return u'/posts/%s/' % self.slug

    # User.objects.send_email_to_subscribers(self)

    class Meta:
        ordering = ('-publication_date',)
        verbose_name = _ul(u'Статья')
        verbose_name_plural = _ul(u'Статьи')

    def __unicode__(self):
        return u'%s' % self.title


class Comment(CommentMixin, MPTTModel, BaseModel):
    parent = TreeForeignKey('self', null=True, blank=True, related_name="comment_parent", verbose_name=_ul(u'Родительский комментарий'))

    post = models.ForeignKey(Post, verbose_name=_ul(u'Статья'))
    author = models.ForeignKey(User, verbose_name=_ul(u'Автор'), null=True, blank=True)
    author_username = models.CharField(verbose_name=_ul(u'Имя автора'), max_length=255)
    comment = RichTextUploadingField(verbose_name=_ul(u'Комментарий'), config_name='comment')
    is_spam = models.BooleanField(verbose_name=_ul(u'Спам?'), default=False)

    rate = models.IntegerField(verbose_name=_ul(u'Кол-во лайков'), default=0)

    def get_absolute_url(self):
        if self.is_published and self.post.is_published:
            return self.post.get_absolute_url() + '#comment_%s' % self.pk

    def save(self, *args, **kwargs):
        if self.author:
            self.author_username = self.author.get_full_name()
        return super(Comment, self).save(*args, **kwargs)

    class Meta:
        ordering = ('-creation_date',)
        verbose_name = _ul(u'Комментарий')
        verbose_name_plural = _ul(u'Комментарии')

    def __unicode__(self):
        return u'%s' % self.comment
