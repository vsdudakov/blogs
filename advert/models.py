#-*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.core.cache import cache
from django.utils.translation import ugettext_lazy as _ul


CACHE_PREFIX = 'advert_'


class Partner(models.Model):
    name = models.CharField(
        verbose_name='Партнерка',
        max_length=255,
        help_text='Название партнерки')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _ul(u'Партнерка')
        verbose_name_plural = _ul(u'Партнерки')


class Advert(models.Model):
    key = models.CharField(
        verbose_name='ключ',
        max_length=255,
        unique=True,
        help_text='Уникальное имя для обращения в шаблоне, лучше использовать латиницу без пробелов')

    content = models.TextField(
        verbose_name='Содержимое',
        help_text='Содержимое блока')

    partner = models.ForeignKey(
        Partner,
        verbose_name="Партнерка",
        help_text="Название партнерки")

    def __unicode__(self):
        return self.key

    def save(self, *args, **kwargs):
        return super(Advert, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _ul(u'Реклама')
        verbose_name_plural = _ul(u'Реклама')