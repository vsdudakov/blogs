#-*- encoding: utf-8 -*-
from django.contrib import admin
from django.contrib.sites.models import Site
from django import forms
from django.conf import settings

from main.admin import BaseModelAdmin
from cms.models import Material, PostMaterial, TextMaterial, PollMaterial, PollItem, Feedback, SiteProfile


class MaterialAdmin(BaseModelAdmin):

    def formfield_for_dbfield(self, db_field, request, *args, **kwargs):
        formfield = super(MaterialAdmin, self).formfield_for_dbfield(db_field, request, *args, **kwargs)
        if db_field.name == 'data' and request.resolver_match.args:
            if Material.objects.filter(
                id=request.resolver_match.args[0], 
                pagetype__in=getattr(settings, 'MATERIAL_TYPES_WITHOUT_WYSIWYG', ())
                ).exists():
                formfield.widget = forms.Textarea()
        return formfield


class PostMaterialAdmin(BaseModelAdmin):
    pass


class TextMaterialAdmin(BaseModelAdmin):
    pass


class PollItemInline(admin.TabularInline):
    model = PollItem


class PollMaterialAdmin(BaseModelAdmin):
    inlines = (PollItemInline,)


class FeedbackAdmin(BaseModelAdmin):
    pass


class SiteProfileInline(admin.StackedInline):
    model = SiteProfile


class SiteAdmin(admin.ModelAdmin):
    inlines = (
        SiteProfileInline,
    )


admin.site.register(Material, MaterialAdmin)
admin.site.register(PostMaterial, PostMaterialAdmin)
admin.site.register(TextMaterial, TextMaterialAdmin)
admin.site.register(PollMaterial, PollMaterialAdmin)
admin.site.register(Feedback, FeedbackAdmin)
admin.site.unregister(Site)
admin.site.register(Site, SiteAdmin)