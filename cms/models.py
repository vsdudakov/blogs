#-*- encoding: utf-8 -*-
import random
from ckeditor_uploader.fields import RichTextUploadingField

from django.db import models
from django.db.models import F, Max
from django.utils.translation import ugettext_lazy as _ul
from django.contrib.sites.models import Site
from django.conf import settings

from main.models import BaseDateModel
from blogs.models import Category, Tag, Post


class Material(BaseDateModel):
    pagetype = models.PositiveSmallIntegerField(verbose_name=_ul(u'Тип страницы'), unique=True, choices=getattr(settings, 'MATERIAL_TYPES'))
    title = models.CharField(verbose_name=_ul(u'Заголовок'), max_length=255)
    data = RichTextUploadingField(verbose_name=_ul(u'Контент'), default="")

    def posts(self):
        post_material = PostMaterial.objects.filter(material=self).first()
        if post_material is not None:
            qs = Post.objects.filter(is_published=True).exclude(category__categorytype=Category.CATEGORY_QUESTIONS)
            filter_catgories = list(post_material.filter_catgories.all())
            if filter_catgories:
                qs = Post.objects.filter(is_published=True)
                qs = qs.filter(category__in=filter_catgories)
            filter_tags = list(post_material.filter_tags.all())
            if filter_tags:
                qs = qs.filter(tags__in=filter_tags)
            if post_material.sort_publication_date:
                qs = qs.order_by('-publication_date')
            if post_material.sort_comments_count:
                qs = qs.filter(comment__isnull=False).annotate(max_comment_creationdate=Max('comment__creation_date'))
                qs = qs.order_by('-max_comment_creationdate')
            if post_material.sort_views_count:
                qs = qs.order_by('-view_count')
            return qs[:post_material.count]
        return []

    def text(self):
        text_material = TextMaterial.objects.filter(material=self).first()
        if text_material is not None:
            materials = self.data.split(text_material.separator)
            if text_material.is_random_show:
                return random.choice(materials)
            if text_material.is_day_show:
                # TODO
                return random.choice(materials)
        return self.data

    def poll(self):
        return PollMaterial.objects.filter(material=self).first()

    class Meta:
        verbose_name = _ul(u'Материал')
        verbose_name_plural = _ul(u'Материалы')

    def __unicode__(self):
        return u'%s' % self.title


class PostMaterial(BaseDateModel):
    material = models.OneToOneField(Material, verbose_name=_ul(u'Материал'))
    filter_catgories = models.ManyToManyField(Category, verbose_name=_ul(u'Фильтровать по категориям'), blank=True)
    filter_tags = models.ManyToManyField(Tag, verbose_name=_ul(u'Фильтровать по меткам'), blank=True)
    sort_publication_date = models.BooleanField(verbose_name=_ul(u'Сортировать по дате публикации'), default=True)
    sort_comments_count = models.BooleanField(verbose_name=_ul(u'Сортировать по кол-ву комментариев'), default=False)
    sort_views_count = models.BooleanField(verbose_name=_ul(u'Сортировать по кол-ву просмотров'), default=False)
    count = models.PositiveIntegerField(verbose_name=_ul(u'Кол-во статей'), default=10)

    class Meta:
        verbose_name = _ul(u'Материал поста')
        verbose_name_plural = _ul(u'Материалы постов')

    def __unicode__(self):
        return u'%s' % self.material.title


class TextMaterial(BaseDateModel):
    material = models.OneToOneField(Material, verbose_name=_ul(u'Материал'))
    is_random_show = models.BooleanField(verbose_name=_ul(u'Показывать рандомно'), default=True)
    is_day_show = models.BooleanField(verbose_name=_ul(u'Показывать рандомно по дням'), default=False)
    separator = models.CharField(verbose_name=_ul(u'Разделитель'), max_length=255, default='<cut/>')

    class Meta:
        verbose_name = _ul(u'Материал текста')
        verbose_name_plural = _ul(u'Материалы текстов')

    def __unicode__(self):
        return u'%s' % self.material.title


class PollMaterial(BaseDateModel):
    material = models.OneToOneField(Material, verbose_name=_ul(u'Материал'))
    question = models.CharField(verbose_name=_ul(u'Вопрос'), max_length=255)

    class Meta:
        verbose_name = _ul(u'Материал опроса')
        verbose_name_plural = _ul(u'Материалы опросов')

    def __unicode__(self):
        return u'%s' % self.material.title


class PollItem(BaseDateModel):
    poll = models.ForeignKey(PollMaterial, verbose_name=_ul(u'Опрос'), related_name="items")
    rate = models.IntegerField(verbose_name=_ul(u'Кол-во за'), default=0)
    answer = models.CharField(verbose_name=_ul(u'Ответ'), max_length=255)

    @property
    def rate_percentage(self):
        total = sum(self.__class__.objects.filter(poll=self.poll).values_list('rate', flat=True))
        if total:
            return int(float(self.rate * 100)/float(total))
        return 0

    def vote_action(self, session):
        if self.poll.has_session_key(session, 'pollitem_vote', setup=True):
            return
        self.__class__.objects.filter(id=self.id).update(rate=F('rate')+1)

    class Meta:
        verbose_name = _ul(u'Ответ')
        verbose_name_plural = _ul(u'Ответы')

    def __unicode__(self):
        return u'%s' % self.answer


class Feedback(BaseDateModel):
    name = models.CharField(verbose_name=_ul(u'Ваше имя'), max_length=255)
    email = models.EmailField(verbose_name=_ul(u'Контактный email'), max_length=255)
    message = RichTextUploadingField(verbose_name=_ul(u'Сообщение'), config_name='feedback')

    class Meta:
        verbose_name = _ul(u'Обратная связь')
        verbose_name_plural = _ul(u'Обратная связь')

    def __unicode__(self):
        return u'%s - %s' % (self.creation_date, self.name)


def get_profile(self):
    try:
        return self.profile
    except Exception:
        return SiteProfile.objects.create(site=self)


Site.add_to_class('get_profile', get_profile)


class SiteProfile(BaseDateModel):
    site = models.OneToOneField(Site, related_name='profile')

    description = models.CharField(max_length=255, null=True, blank=True)
    seo_title = models.CharField(max_length=255, null=True, blank=True)
    seo_keywords = models.CharField(max_length=255, null=True, blank=True)
    seo_description = models.CharField(max_length=255, null=True, blank=True)
    seo_author = models.CharField(max_length=255, null=True, blank=True)
    stat_liveinternet = models.TextField(null=True, blank=True)
    stat_yandex = models.TextField(null=True, blank=True)
    stat_google = models.TextField(null=True, blank=True)
    yandex_verification = models.CharField(max_length=255, null=True, blank=True)
    google_verification = models.CharField(max_length=255, null=True, blank=True)
    important_message = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        verbose_name = _ul(u'Профайл сайта')
        verbose_name_plural = _ul(u'Профайлы сайтов')

    def __unicode__(self):
        return u"%s" % self.site


