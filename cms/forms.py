#-*- encoding: utf-8 -*-
from django import forms

from cms.models import Feedback
from main.forms import BootstrapModelForm


class FeedbackForm(BootstrapModelForm):

    class Meta:
        model = Feedback
        fields = ('name', 'email', 'message')
