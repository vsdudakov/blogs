#-*- encoding: utf-8
from django.core.management.base import BaseCommand
from django.core.management import call_command
from django.conf import settings


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        call_command('migrate', interactive=False)
        call_command('collectstatic', interactive=False)
        call_command('compilemessages', interactive=False)
        call_command('sync_comments', interactive=False)
        if not settings.DEBUG:
            call_command('compress', interactive=False)
