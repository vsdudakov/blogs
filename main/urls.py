from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import RedirectView
from django.core.urlresolvers import reverse_lazy
from django.conf import settings
from django.conf.urls.static import static

from users import views as users_views
from blogs import views as blogs_views
from cms import views as cms_views
from main import views as main_views


urlpatterns = [
    url(r'^$', blogs_views.IndexView.as_view(), name='index'),

    url(r'^poslednie-statyi/$', blogs_views.PostChoicesListView.as_view(is_last=True), name='posts-last-list'),
    url(r'^popularnye-statyi/$', blogs_views.PostChoicesListView.as_view(is_populate=True), name='posts-popular-list'),
    url(r'^kommentiruemye-statyi/$', blogs_views.PostChoicesListView.as_view(is_commented=True), name='posts-commented-list'),
    url(r'^poisk/$', blogs_views.PostSearchListView.as_view(), name='posts-search'),
    url(r'^kontakty/$', cms_views.FeedbackCreateView.as_view(), name='feedback'),
    url(r'^reklamodatelyam/$', cms_views.FlatPageDetailView.as_view(), name='advertisers'),
    url(r'^pravila-i-avtorskie-prava/$', cms_views.FlatPageDetailView.as_view(), name='rules'),

    url(r'^posts/$', blogs_views.PostListView.as_view(), name='posts-list'),
    url(r'^posts/(?P<slug>[-_\w]+)/$', blogs_views.PostCategoryDetailView.as_view(), name='posts-category-detail'),
    url(r'^category/(?P<category_slug>[-_\w]+)/$', blogs_views.PostCategoryListView.as_view(), name='posts-category-list'),
    
    url(r'^u/login/$', users_views.LoginView.as_view(), name='u-login'),
    url(r'^u/logout/$', users_views.LogoutView.as_view(), name='u-logout'),
    url(r'^u/registration/$', users_views.RegistrationView.as_view(), name='u-registration'),
    url(r'^u/registration/(?P<uuid>.*)/$', users_views.RegistrationUuidView.as_view(), name='u-registration-uuid'),
    url(r'^u/reset/password/$', users_views.PasswordResetView.as_view(), name='u-reset-password'),
    url(r'^u/reset/password/(?P<uuid>.*)/$', users_views.PasswordResetUuidView.as_view(), name='u-reset-password-uuid'),
    url(r'^u/profile/(?P<pk>\d+)/$', users_views.UserDetailView.as_view(), name='u-profile'),
    url(r'^u/update/(?P<pk>\d+)/$', users_views.UserUpdateView.as_view(), name='u-update'),
    url(r'^u/password/$', users_views.PasswordChangeView.as_view(), name='u-password'),
    url(r'^u/subscribe/$', users_views.SubscribeUser.as_view(), name='subscribe-user'),
    
    url(r'^u/comments/$', blogs_views.CommentUserListView.as_view(), name='u-comments-list'),
    url(r'^json/comment/$', blogs_views.comment_json, name='json-comment'),
    url(r'^json/comment/like/$', blogs_views.comment_like, name='json-comment-like'),
    url(r'^json/comment/unlike/$', blogs_views.comment_unlike, name='json-comment-unlike'),
    url(r'^json/post/like/$', blogs_views.post_like, name='json-post-like'),
    url(r'^json/post/unlike/$', blogs_views.post_unlike, name='json-post-unlike'),
    url(r'^json/cms/vote/$', cms_views.cms_vote, name='json-cms-vote'),

    url(r'^u/flush/$', main_views.flush_cache),
    url(r'^u/admin/', include(admin.site.urls)),
    url(r'^u/ckeditor/upload/', main_views.ImageUploadView.as_view(), name='ckeditor_upload'),
    url(r'^u/ckeditor/', include('ckeditor_uploader.urls')),

    url(r'^robots\.txt$', main_views.RobotTemplateView.as_view()),
    url(r'^sitemap\.xml$', main_views.sitemap_xml),
    url(r'^sitemap-(?P<section>.+)\.xml$', main_views.sitemap_xml),
    url(r'^sitemap\.html$', main_views.sitemap_html, name='sitemap-html'),
    url(r'^feed\.rss$', main_views.feed, name='feed'),

    url(r'^m/$', RedirectView.as_view(url=reverse_lazy('index'))),
    url(r'^mobile/$', RedirectView.as_view(url=reverse_lazy('index'))),
    url(r'^posts/tag/(?P<tag_slug>[-_\w]+)/$', RedirectView.as_view(url=reverse_lazy('index'))),
    url(r'^karta-sajta\.html$', RedirectView.as_view(url=reverse_lazy('sitemap-html'))),
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/favicon.ico')),
    
    url(r'^forum/', include('djangobb_forum.urls', namespace='djangobb'))
]


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)