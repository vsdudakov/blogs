#-*- encoding: utf-8 -*-
from ckeditor_uploader.fields import RichTextUploadingField

from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _ul, ugettext as _u


class BaseDateModel(models.Model):
    creation_date = models.DateTimeField(verbose_name=_ul(u'Дата создания'), auto_now_add=True)
    modification_date = models.DateTimeField(verbose_name=_ul(u'Дата изменения'), auto_now=True)

    def has_session_key(self, session, key, setup=False):
        key = key + '_%s' % self.id
        result = session.get(key, False)
        if setup:
            session[key] = True
        return result

    class Meta:
        abstract = True


class BaseModel(BaseDateModel):
    is_published = models.BooleanField(verbose_name=_ul(u'Опубликован?'), default=True, db_index=True)

    class Meta:
        abstract = True


class BaseSeoModel(BaseModel):
    slug = models.SlugField(verbose_name=_ul(u'Слаг'), unique=True, max_length=255)

    seo_title = models.CharField(verbose_name=_ul(u'Заголовок (meta тег)'), max_length=255, null=True, blank=True)
    seo_keywords = models.CharField(verbose_name=_ul(u'Ключевые слова (meta тег)'), max_length=255, null=True, blank=True)
    seo_description = models.CharField(verbose_name=_ul(u'Описание (meta тег)'), max_length=255, null=True, blank=True)
    seo_author = models.CharField(verbose_name=_ul(u'Автор (meta тег)'), max_length=255, null=True, blank=True)

    class Meta:
        abstract = True


class BasePostModel(BaseSeoModel):
    title = models.CharField(verbose_name=_ul(u'Заголовок'), max_length=255, unique=True)
    announcement = RichTextUploadingField(verbose_name=_ul(u'Анонс'))
    post = RichTextUploadingField(verbose_name=_ul(u'Статья'))

    view_count = models.IntegerField(verbose_name=_ul(u'Кол-во просмотров'), default=0)
    rate = models.IntegerField(verbose_name=_ul(u'Кол-во лайков'), default=0)
    num_comments = models.IntegerField(verbose_name=_ul(u'Кол-во комментариев'), default=0)

    publication_date = models.DateTimeField(verbose_name=_ul(u'Дата публикации'), null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.publication_date is None and self.is_published:
            self.publication_date = timezone.now()
        return super(BasePostModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True
