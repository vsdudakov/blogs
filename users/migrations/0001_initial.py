# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-31 20:18
from __future__ import unicode_literals

import ckeditor_uploader.fields
import django.contrib.auth.validators
from django.db import migrations, models
import django.utils.timezone
import users.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.ASCIIUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=30, verbose_name='last name')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('nickname', models.CharField(blank=True, max_length=255, null=True, verbose_name='\u041d\u0438\u043a')),
                ('avatar', models.FileField(default=b'defaults/user.jpeg', upload_to=b'users/', verbose_name='\u0410\u0432\u0430\u0442\u0430\u0440')),
                ('about', ckeditor_uploader.fields.RichTextUploadingField(blank=True, null=True, verbose_name='\u041e \u0441\u0435\u0431\u0435')),
                ('city', models.CharField(blank=True, default=b'', max_length=128, verbose_name='\u0413\u043e\u0440\u043e\u0434')),
                ('reset_password_key', models.CharField(blank=True, max_length=255, null=True, unique=True, verbose_name='\u041a\u043b\u044e\u0447 \u0441\u0431\u0440\u043e\u0441 \u043f\u0430\u0440\u043e\u043b\u044f')),
                ('registration_key', models.CharField(blank=True, max_length=255, null=True, unique=True, verbose_name='\u041a\u043b\u044e\u0447 \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044f')),
                ('is_subscribe', models.BooleanField(default=False, verbose_name='\u041f\u043e\u0434\u043f\u0438\u0441\u0430\u043d')),
                ('date_subscribe', models.DateTimeField(blank=True, null=True, verbose_name='\u0414\u0430\u0442\u0430 \u043f\u043e\u0434\u043f\u0438\u0441\u043a\u0438')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            managers=[
                ('objects', users.models.ExtUserManager()),
            ],
        ),
    ]
