#-*- encoding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _ul

from users.models import User


class UserAdmin(BaseUserAdmin):
    fieldsets = BaseUserAdmin.fieldsets + (
        (None, {
            'fields': (
                'avatar',
                'about',
                'city',
            )
        }),
        (_ul(u'Ключи'), {
            'fields': (
                'reset_password_key',
                'registration_key',
                )
        }),
        (_ul(u'Подписка'), {
            'fields': (
                'is_subscribe',
                'date_subscribe',
                )
        }),
    )
    readonly_fields = (
        'reset_password_key',
        'registration_key',
    )
    list_display = BaseUserAdmin.list_display + (
        'is_active',
        'is_subscribe',
    )


admin.site.register(User, UserAdmin)
