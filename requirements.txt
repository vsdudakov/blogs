django==1.10.7
django-ckeditor==5.2.1
django-mptt==0.8.7
django-compressor==2.1.1

sorl-thumbnail==12.3
pillow==4.1.0
python-slugify==1.2.2
beautifulsoup==3.2.1

pytz==2017.2
postmarkup==1.2.2
haystack==0.36
django_haystack==2.6.0

psycopg2==2.7.1
uwsgi==2.0.15
fabric==1.13.1
